package com.technomanci.fundraiserms.Fundraiser.exceptions;

public class FundraiserNotFoundException extends RuntimeException {
    public FundraiserNotFoundException(Integer id) {
        super("Could not find Employee " + id);
    }
}