package com.technomanci.fundraiserms.Fundraiser;

import org.springframework.data.repository.CrudRepository;

import com.technomanci.fundraiserms.Fundraiser.Fundraiser;

public interface FundraiserRepository extends CrudRepository<Fundraiser, Integer> {
    
}