package com.technomanci.fundraiserms.Fundraiser;

import java.util.Set;

/**
 * Service object for fetching and persisting Fundraisers from the Datastore
 */
public interface FundraiserService {
    /**
     * Fetch a single Fundraiser from the datastore, by ID.
     * @param fundraiserId
     * @return FundraiserDTO fundraiser data object
     */
    FundraiserDTO findFundraiserById(String fundraiserId);
    Set<FundraiserDTO> getFundraisersByUserId(String userId);
    Set<FundraiserDTO> getActiveFundraisers();
    Set<FundraiserDTO> getInactiveFundraisers(boolean includeUnfinished);
    Set<FundraiserDTO> getUnfinishedFundraisers();
}