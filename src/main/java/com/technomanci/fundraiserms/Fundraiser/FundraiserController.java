package com.technomanci.fundraiserms.Fundraiser;

import javax.ws.rs.GET;

import com.technomanci.fundraiserms.Fundraiser.exceptions.FundraiserNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FundraiserController {
	@Autowired
	private FundraiserRepository fundraiserRepository;

	@PostMapping(path = "/fundraiser")
	public Fundraiser createFundraiser(@RequestBody Fundraiser newFundraiser) {
		return fundraiserRepository.save(newFundraiser);
	}

	@GetMapping(path = "/fundraisers")
	public @ResponseBody Iterable<Fundraiser> getAllFundraisers() {
		// This returns a JSON or XML with the users
		return fundraiserRepository.findAll();
	}

	@GetMapping(path = "/fundraisers/{id}")
	public Fundraiser readFundraiser(@PathVariable("id") Integer id) {
		return fundraiserRepository.findById(id).orElseThrow(() -> new FundraiserNotFoundException(id));
	}
}