package com.technomanci.fundraiserms.Fundraiser;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@ToString
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FundraiserDTO {
    private String id;
    private String userUUID;
    private String campaignUUID;
    private String title;
    private String description;
    private int goal;
    private Date ends;
    private String imageUUID;
    

}