package com.technomanci.fundraiserms.Fundraiser.advice;

import com.technomanci.fundraiserms.Fundraiser.exceptions.FundraiserNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class FundraiserNotFoundAdvice  extends ResponseEntityExceptionHandler {
    @ResponseBody
    @ExceptionHandler(FundraiserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String fundraiserNotFoundHandler(FundraiserNotFoundException ex) {
        return ex.getMessage();
    }
}